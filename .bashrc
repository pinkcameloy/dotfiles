#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls="ls --color=auto"
alias ll="ls -lAh"
alias syu="sudo pacman -Syu"
alias mv='mv -i'
alias rm='rm -i'
alias menuupd="mmaker openbox -f -t xterm" #openbox reconfigure necessary
alias rxvtupd="xrdb ~/.Xresources"
alias mc='. /usr/lib/mc/mc-wrapper.sh'
PS1='[\u@\h \W]\$ '
alias configbu='/usr/bin/git --git-dir=/home/gybe/dotfiles --work-tree=/home/gybe'
