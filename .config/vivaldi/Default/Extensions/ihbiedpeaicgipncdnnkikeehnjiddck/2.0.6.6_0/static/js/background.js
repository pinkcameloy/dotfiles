const API_URL = "https://api.weather-extension.com";
const HP_URL = "https://www.weather-extension.com";
if (!localStorage.units) {
  localStorage.units = "c";
}

if (!localStorage.default_city) {
  localStorage.default_city = "";
}

if (!localStorage.default_loc) {
  localStorage.default_loc = "";
}

if (!localStorage.default_DisplayName) {
  localStorage.default_DisplayName = "";
}

//GA implemented
const details = chrome.runtime.getManifest();

(function (i, s, o, g, r, a, m) {
  i["GoogleAnalyticsObject"] = r;
  (i[r] =
    i[r] ||
    function () {
      (i[r].q = i[r].q || []).push(arguments);
    }),
    (i[r].l = 1 * new Date());
  (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
  a.async = 1;
  a.src = g;
  m.parentNode.insertBefore(a, m);
})(
  window,
  document,
  "script",
  "https://www.google-analytics.com/analytics.js",
  "ga"
);

ga("create", "UA-130709675-1", "auto");
ga("set", "checkProtocolTask", function () { });
ga("require", "displayfeatures");
ga("send", "pageview", "background.html?v=" + details.version);

// Restart timer because of unknown behavior
GLOBAL_TIMER_26 = setTimeout(function () {
  window.location.reload();
}, 21600 * 1000);

const deg = "°";
const getCel = e => {
  return (5 / 9) * (e - 32);
};

function refreshData() {
  let city_loc = localStorage.loc,
    city_name = localStorage.city;

  if (!city_loc) {
    city_loc = localStorage.default_loc;
    city_name = localStorage.default_city;
  }

  var uri = `${API_URL}/current?location=` + city_loc + "&city=" + city_name;

  if (city_loc === "" && city_name === "") {
    uri = uri.replace("&city=", "");
  }

  var uriEncoded = encodeURI(uri);

  $.getJSON(uriEncoded, { format: "json" })
    .done(function (res) {
      var temp = res && res.main && res.main.temp ? res.main.temp : null;
      if (!temp) {
        return;
      }

      var result = res.weather[0];

      const units = localStorage.units;
      const description = result.description;
      const color = getColor(description);
      const icon = getIcon(description);
      if (units === "c") {
        temp = getCel(temp);
      }

      temp = Math.round(temp);

      chrome.browserAction.setBadgeBackgroundColor({ color });
      const unit = units === "c" ? "C" : "F";
      chrome.browserAction.setBadgeText({
        text: temp <= -10 ? temp + unit : temp + deg.substring(1) + unit
      });
      chrome.browserAction.setIcon({
        path:
          "static/icons/small/" +
          (color === "#8EB0D1" ? "blue/" : "orange/") +
          icon
      });
    })
    .fail(() => { });
}

window.setTimeout(refreshData, 3000);

window.setInterval(refreshData, 300000);

function getIcon(e) {
  switch (true) {
    case /^.*clear sky.*$/.test(e):
      return "clear-sky-o.png";
    case /^.*few clouds.*$/.test(e):
      return "few-clouds-o.png";
    case /^.*scattered clouds.*$/.test(e):
      return "Shape_11.png";
    case /^.*broken clouds.*$/.test(e):
      return "Shape_11.png";
    case /^.*shower rain.*$/.test(e):
      return "shower-rain-o.png";
    case /^.*rain.*$/.test(e):
      return "rain-o.png";
    case /^.*thunderstorm.*$/.test(e):
      return "thunderstorm-o.png";
    case /^.*snow.*$/.test(e):
      return "snowflake-o.png";
    case /^.*mist.*$/.test(e):
      return "mist-o.png";
    default:
      return "clear-sky-o.png";
  }
}

function getColor(description) {
  switch (true) {
    case /^.*clear sky.*$/.test(description):
      return "#F1B385";
    case /^.*few clouds.*$/.test(description):
      return "#F1B385";
    default:
      return "#8EB0D1";
  }
}

var installUrl = `${HP_URL}/welcome`;
var uninstallUrl = `${HP_URL}/uninstalled`;

chrome.runtime.onInstalled.addListener(function (details) {
  if ((details.reason = "install")) {
    chrome.tabs.create({ url: installUrl });
  }
});

chrome.runtime.setUninstallURL(uninstallUrl);
