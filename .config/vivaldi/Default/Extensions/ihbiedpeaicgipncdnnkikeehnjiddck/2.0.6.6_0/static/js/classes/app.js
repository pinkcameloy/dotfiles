define(['classes/gpaph', 'classes/options', 'classes/daysTable'], function(
  Graph,
  OptionsDialog,
  DaysTable
) {
  const API_URL_PROD = 'https://api.weather-extension.com/';

  class App {
    constructor() {
      this.weatherDataList = [];
      this.color = '';
      this.optionsDialog = new OptionsDialog();
    }

    getIcon(e) {
      switch (true) {
        case /^.*clear sky.*$/.test(e):
          return 'clear-sky-o.svg';
        case /^.*few clouds.*$/.test(e):
          return 'few-clouds-o.svg';
        case /^.*scattered clouds.*$/.test(e):
          return 'Shape_11.svg';
        case /^.*broken clouds.*$/.test(e):
          return 'Shape_11.svg';
        case /^.*shower rain.*$/.test(e):
          return 'shower-rain-o.svg';
        case /^.*rain.*$/.test(e):
          return 'rain-o.svg';
        case /^.*thunderstorm.*$/.test(e):
          return 'thunderstorm-o.svg';
        case /^.*snow.*$/.test(e):
          return 'snowflake-o.svg';
        case /^.*mist.*$/.test(e):
          return 'mist-o.svg';
        case /^.*overcast clouds.*$/.test(e):
          return 'Shape_11.svg';
        default:
          return 'clear-sky-o.svg';
      }
    }

    getColor(description) {
      switch (true) {
        case /^.*clear sky.*$/.test(description):
          return '#F1B385';
        case /^.*few clouds.*$/.test(description):
          return '#F1B385';
        default:
          return '#8EB0D1';
      }
    }
    getBackgroundImage(icon) {
      switch (icon) {
        case '01d':
          return 'Clear-Sky-Day-3.jpg';
        case '01n':
          return 'Clear-Sky-Night.jpg';
        case '02d':
          return 'Partly-Cloudy-Day.jpg';
        case '02n':
          return 'Partly-Cloudy-Night.jpg';
        case '03d':
          return 'Scattered-Clouds.jpg';
        case '03n':
          return 'Scattered-Clouds-Night.jpg';
        case '04d':
          return 'Broken-Clouds-Day.jpg';
        case '04n':
          return 'Broken-Clouds-Night.jpg';
        case '09d':
          return 'Shower-Rain-Day.jpg';
        case '09n':
          return 'Shower-Rain-Night.jpg';
        case '10d':
          return 'Rain-Day.jpg';
        case '10n':
          return 'Rain-Night.jpg';
        case '11d':
          return 'Thunderstorm-Day.jpg';
        case '11n':
          return 'Thunderstorm-Night.jpg';
        case '13d':
          return 'Snow-Day.jpg';
        case '13n':
          return 'Snow-Night.jpg';
        case '50d':
          return 'Mist-Day.jpg';
        case '50n':
          return 'Mist-Night.jpg';
        default:
          return 'Clear-Sky-Day-3.jpg';
      }
    }
    getWeekDay(e) {
      return [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ][e];
    }
    formatAMPM(e) {
      var a = e.getHours(),
        t = (e.getMinutes(), a >= 12 ? 'pm' : 'am');
      return (a % 12 || 12) + t;
    }

    getCel(e) {
      return (5 / 9) * (e - 32);
    }
    getChart(hourlyData, chartType) {
      var myChart = new Graph($('#myChart'));
      myChart.setData(hourlyData, chartType);
      myChart.setStyle(hourlyData[0].color);
    }
    setForecastData(currData, units, city_name) {
      this.color = this.getColor(currData.weather[0].description);
      $('#conditions').css('color', this.color);
      var currTempMin = Math.round(
        Math.min(...this.weatherDataList.slice(0, 8).map(x => x.main.temp_min))
      );
      var currTemp = Math.round(currData.main.temp);
      console.log(currTemp);
      var currTempMax = Math.round(
        Math.max(...this.weatherDataList.slice(0, 8).map(x => x.main.temp_max))
      );
      if (units === 'c') {
        currTempMin = Math.round(
          this.getCel(
            Math.min(
              ...this.weatherDataList.slice(0, 8).map(x => x.main.temp_min)
            )
          )
        );
        currTempMax = Math.round(
          this.getCel(
            Math.max(
              ...this.weatherDataList.slice(0, 8).map(x => x.main.temp_max)
            )
          )
        );
        currTemp = Math.round(this.getCel(currData.main.temp));
      }
      var iconCurrTemp = this.getIcon(currData.weather[0].description);
      //aside section
      $('#conditions')
        .append(
          '<div class="temp">' +
            '<img class="icon" src="static/icons/' +
            (this.color === '#8EB0D1' ? 'blue/' : 'orange/') +
            iconCurrTemp +
            '"></img><div id="title">Today</div></div>'
        )
        .append(
          '<div class="details"><span id="currMin">' +
            currTempMin +
            '&deg;</span> /<span id="currMax"> ' +
            currTempMax +
            '</span>&deg; </div>'
        );
    }
    setCurrData(currData, units, city_name) {
      this.color = this.getColor(currData.weather[0].description);

      var currTempMin = Math.round(
        Math.min(...this.weatherDataList.slice(0, 8).map(x => x.main.temp_min))
      );
      var currTemp = Math.round(currData.main.temp);
      console.log(currTemp);
      var currTempMax = Math.round(
        Math.max(...this.weatherDataList.slice(0, 8).map(x => x.main.temp_max))
      );
      if (units === 'c') {
        currTempMin = Math.round(
          this.getCel(
            Math.min(
              ...this.weatherDataList.slice(0, 8).map(x => x.main.temp_min)
            )
          )
        );
        currTempMax = Math.round(
          this.getCel(
            Math.max(
              ...this.weatherDataList.slice(0, 8).map(x => x.main.temp_max)
            )
          )
        );
        currTemp = Math.round(this.getCel(currData.main.temp));
      }
      //aside section

      $('#imgWrapper').attr(
        'style',
        `background-image:url("static/icons/background/${this.getBackgroundImage(
          currData.weather[0].icon
        )}")`
      );
      $('#currTemp').append(
        `<h1>${currTemp}°</h1><h3>${currData.weather[0].description}</h3>`
      );
      //temperature in extension icon
      chrome.browserAction.setBadgeBackgroundColor({ color: this.color });
      $('#location').text(
        'in ' + (city_name !== '' ? city_name : currData.city.name)
      );
    }
    get_weather_info() {
      var city_loc = localStorage.loc,
        city_name = localStorage.city,
        city_display_name = localStorage.DisplayName;
      if (!city_loc) {
        city_loc = localStorage.default_loc;
        city_name = localStorage.default_city;
        city_display_name = localStorage.default_DisplayName;
      }

      var uri =
        `${API_URL_PROD}current?location=` + city_loc + '&city=' + city_name;

      if (city_loc === '' && city_name === '') {
        uri = uri.replace('&city=', '');
      }

      var uriEncoded = encodeURI(uri);

      $.getJSON(uriEncoded, { format: 'json' }).done(function(res) {
        const units = localStorage.units;
        self.setCurrData(res, units, city_name);
      });

      var uri =
        `${API_URL_PROD}forecast?location=` + city_loc + '&city=' + city_name;
      var mapsApi = encodeURI(uri);
      var self = this;
      $.getJSON(mapsApi, { format: 'json' })

        .done(function(resData) {
          if (!resData || !resData.list) {
            return;
          }

          if (city_name === '') {
            localStorage.default_DisplayName =
              resData.city.name + resData.city.country;
            localStorage.default_city = resData.city.name;
            city_name = resData.city.name;
          }
          var units = localStorage.units; //a
          self.weatherDataList = resData.list;
          setTimeout(
            self.getChart(
              self.weatherDataList.slice(0, 8).map(x => {
                return {
                  icon: x.weather[0].description,
                  temp: x.main.temp,
                  dt: self.formatAMPM(new Date(1e3 * x.dt)),
                  rain: x.rain,
                  snow: x.snow,
                  color: self.getColor(x.weather[0].description)
                };
              }),
              $('#rainButton').hasClass('showRain')
            ),
            2000
          );
          //set current data aside and conditions
          self.setForecastData(self.weatherDataList[0], units, city_name);
          //days table
          self.setDaysTable(self.weatherDataList, city_loc, units);
          //fade-in effect main content
          self.showContent();
          self.optionsDialog.setColor(self.color);
        })
        .fail(function() {
          $('#online, #conditions').fadeOut('slow', function() {
            $('#offline').fadeIn('slow');
          });
        });
    }
    setDaysTable(weatherDataList, city_loc, units) {
      var daysTable = new DaysTable($('#weather'));
      var weekData = weatherDataList
        .slice(8, 32)
        .filter((el, index) => index % 8 === 0);
      var weekDays = weekData.map(x =>
        this.getWeekDay(new Date(1e3 * x.dt).getDay())
      );
      var temps = weekData.map(x => {
        if (units === 'c') {
          x = Math.round(this.getCel(x.main.temp));
        } else {
          x = Math.round(x.main.temp);
        }
        return x;
      });
      var iconsDaily = weekData.map(x =>
        this.getIcon(x.weather[0].description)
      );
      daysTable.setData(weekDays, temps, iconsDaily, this.color, city_loc);
    }
    showContent() {
      $('#loading').fadeOut('fast', function() {
        $('#conditions,#myChart,.weatherContainer').hide();
        $('#online').fadeIn();
        $('#conditions').fadeIn({ duration: 1000 });
        $('#myChart').fadeIn({ duration: 2000 });
        $('.weatherContainer:nth-child(1)').fadeIn({ duration: 2300 });
        $('.weatherContainer:nth-child(2)').fadeIn({ duration: 2600 });
        $('.weatherContainer:last-child').fadeIn({ duration: 2900 });
      });
    }
    showHideRainTemp() {
      var button = $('#rainButton');
      button.click(() => {
        button.toggleClass('showRain');
        var chartType;
        if (button.hasClass('showRain')) {
          button.html(
            'What about the temperature? <img src="static/icons/icon_sun_16.png" />'
          );
          button.css('left', 22);
          chartType = 'rain';
        } else {
          button.html(
            'But it is going to rain? <img src="static/icons/icon_rain_16.png" />'
          );
          button.css('left', 51);
          chartType = 'temp';
        }
        if ($('#chartjs-tooltip').first()) {
          $('#chartjs-tooltip').empty();
        }
        this.getChart(
          this.weatherDataList.slice(0, 8).map(x => {
            return {
              icon: x.weather[0].description,
              temp: x.main.temp,
              dt: this.formatAMPM(new Date(1e3 * x.dt)),
              rain: x.rain,
              snow: x.snow,
              color: this.getColor(x.weather[0].description)
            };
          }),
          chartType
        );
      });
    }
    showHideOptions() {
      $('#options_btn').click(() => {
        this.optionsDialog.open(this.color).then(data => {
          if (data) {
            this.get_weather_info();
          }
          this.getChart(
            this.weatherDataList.slice(0, 8).map(x => {
              return {
                icon: x.weather[0].description,
                temp: x.main.temp,
                dt: this.formatAMPM(new Date(1e3 * x.dt)),
                rain: x.rain,
                snow: x.snow,
                color: this.getColor(x.weather[0].description)
              };
            }),
            $('#rainButton').hasClass('showRain')
          );
        });
      });
      this.optionsDialog.setFahCel();
      this.optionsDialog.setTravelWeather();
    }
  }
  return App;
});
