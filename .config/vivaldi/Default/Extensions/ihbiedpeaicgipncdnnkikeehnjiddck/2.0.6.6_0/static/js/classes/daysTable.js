define(function() {
  class DaysTable {
    constructor(htmlEl) {
      this.htmlEl = htmlEl;
    }
    setData(weekDays, temps, iconsDaily, color, city_loc) {
      weekDays.forEach((el, i) => {
        this.htmlEl.append(
          '<div class="weatherContainer"><a target="_blank" href="https://openweathermap.org/' +
            "?lat=" +
            city_loc.split(",")[0] +
            "&lon=" +
            city_loc.split(",")[1] +
            '"><span class="date">' +
            el +
            '</span><img class="icon" src="static/icons/' +
            (color === "#8EB0D1" ? "blue/" : "orange/") +
            iconsDaily[i] +
            '"></img><span class="weatherTemp">' +
            temps[i] +
            "&deg;</span></a></div>"
        );
      });
      $(".weatherTemp").css("color", color);
      $(".weatherContainer").css("border-bottom", " 0.5px solid" + color);
      $(".weatherContainer:last-child").css("border-bottom", "none");
    }
  }
  return DaysTable;
});
