
define(["classes/app"], function(App) {
  var app = new App();
  app.showHideRainTemp();
  app.showHideOptions();
  $("#loading").fadeIn("fast");
  app.get_weather_info();
});
