define(["lib/Chart.bundle.min"], function(Chart) {
  class Graph extends Chart {
    constructor(canvasEl) {
      super(canvasEl, {
        type: "line",
        defaultFontFamily: "Neuzeit Grotesk",
        plugins: [
          {
            beforeRender: function(chart) {
              if (chart.config.options.showAllTooltips) {
                // create an array of tooltips
                // we can't use the chart tooltip because there is only one tooltip per chart
                chart.pluginTooltips = [];
                chart.config.data.datasets.forEach(function(dataset, i) {
                  chart.getDatasetMeta(i).data.forEach(function(sector, j) {
                    chart.pluginTooltips.push(
                      new Chart.Tooltip({
                        _chart: chart.chart,
                        _chartInstance: chart,
                        _data: chart.data,
                        _options: chart.options.tooltips,
                        _active: [sector]
                      })
                    );
                  });
                });

                // turn off normal tooltips
                chart.options.tooltips.enabled = false;
              }
            }
          }
        ],
        options: {
          events: [],
          scales: {
            xAxes: [
              {
                display: false,
                type: "category",
                gridLines: {
                  display: false
                },
                offset: 1
              }
            ],
            yAxes: [
              {
                display: false,
                gridLines: {
                  display: false
                }
              }
            ]
          },
          legend: {
            display: false
          },
          showAllTooltips: true,
          tooltips: {
            enabled: false,
            callbacks: {
              label: function(tooltipItems, data) {
                return tooltipItems.yLabel + "°";
              }
            }
          }
        }
      });
    }
    setStyle(color) {
      this.config.plugins[0].afterDraw = function(chart, easing) {
        if (chart.config.options.showAllTooltips) {
          // we don't want the permanent tooltips to animate, so don't do anything till the animation runs atleast once
          if (!chart.allTooltipsOnce) {
            if (easing !== 1) return;
            chart.allTooltipsOnce = true;
          }
          var tooltipEl = $("#chartjs-tooltip");
          var position = chart.canvas.getBoundingClientRect();
          var data = chart.getDatasetMeta(0).data;
          var xLables = chart.options.scales.xAxes[0].labels;
          var yLables = chart.config.data.datasets[0].data;

          var count = 0;

          if ($("#chartjs-tooltip").first()) {
            $("#chartjs-tooltip").empty();
          }
        
          data.forEach(point => {
            var spanY = $("#rainButton").hasClass('showRain')
              ? $("<span></span>")
                  .text(yLables[count])
                  .css("color", "#5DB4DD")
                  .append("<span>mm<sup>3</sup> </span>")
                  .addClass("yLabel")
              : $("<span></span>")
                  .text(yLables[count] + "°")
                  .addClass("yLabel")
                  .css("color", color);

            var spanX = $("<span></span>")
              .text(xLables[count])
              .addClass("xLabel");
            spanY.css({ position: "absolute" });
            if ($("#rainButton").attr("class")) {
              if (count === 0) {
                spanY.css({
                  left: point._model.x - 5 + "px"
                });
              } else {
                spanY.css({
                  left: point._model.x - 20 + "px"
                });
              }
            } else {
              spanY.css({
                left: point._model.x - 5 + "px"
              });
            }

            spanY.css({
              top: position.top - 30 + point._model.y + "px"
            });

            spanX.css({ position: "absolute" });

            spanX.css({
              left: point._model.x - 10 + "px"
            });

            spanX.css({
              top: position.top + 20 + point._model.y + "px"
            });

            tooltipEl.append(spanY);
            tooltipEl.append(spanX);
            count++;
          });

          chart.config.options.showAllTooltips = false;
        }
      };
    }
    formatRainData(hourlyData) {
      console.log("yes");
      console.log(hourlyData);
      return [
        {
          //{} , 3h :{ 0.003}
          fill: false,
          pointBackgroundColor: "#5DB4DD",
          pointRadius: 4.5,
          borderColor: "#5DB4DD",
          data: hourlyData
            .map(x =>
              x.rain === undefined
                ? 0
                : Object.keys(x.rain).length === 0
                ? 0
                : x.rain["3h"].toFixed(1)
            )
            .slice(0, 5)
        }
      ];
    }
    formatTempData(hourlyData) {
      return [
        {
          fill: false,
          pointBackgroundColor: hourlyData[0].color,
          pointRadius: 4.5,
          borderColor: hourlyData[0].color,
          data: hourlyData
            .map(x => {
              return localStorage.units === "c"
                ? Math.round((5 / 9) * (x.temp - 32))
                : Math.round(x.temp);
            })
            .slice(0, 5)
        }
      ];
    }
    setData(hourlyData, chartType) {
      var isRain = chartType === "rain";
      var chartData = [];
      if (isRain) {
        chartData = this.formatRainData(hourlyData);
      } else {
        chartData = this.formatTempData(hourlyData);
      }
      this.config.data.datasets = chartData;
      this.config.options.scales.xAxes[0].labels = hourlyData
        .map(x => x.dt)
        .slice(0, 5);
      this.config.options.scales.yAxes[0].beforeFit = function(scale) {
        if (
          scale.chart.config &&
          scale.chart.config.data &&
          scale.chart.config.data.datasets
        ) {
          var minValue = Math.min(...scale.chart.config.data.datasets[0].data);
          var maxValue = Math.max(...scale.chart.config.data.datasets[0].data);
        }
        if (Math.abs(maxValue) >= Math.abs(minValue)) {
          if (maxValue < 0 && minValue < 0) {
            scale.options.ticks.max = -2 * maxValue;
            scale.options.ticks.min = 3 * maxValue;
          } else {
            scale.options.ticks.max = 4 * maxValue;
            scale.options.ticks.min = -2 * maxValue;
          }
        } else {
          if (maxValue < 0) {
            scale.options.ticks.max = -0.5 * minValue;
            scale.options.ticks.min = 1.5 * minValue;
          } else {
            scale.options.ticks.max = -1.5 * minValue;
            scale.options.ticks.min = 1.5 * minValue;
          }
        }
      };
      this.update();
    }
  }
  return Graph;
});
