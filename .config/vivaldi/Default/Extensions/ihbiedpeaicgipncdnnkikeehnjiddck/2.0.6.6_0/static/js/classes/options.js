define(function() {
  class OptionsDialog {
    debounce(func, wait, immediate) {
      let timeout;
      return function() {
        let context = this;
        let args = arguments;
        let later = function() {
          timeout = null;
          if (!immediate) {
            func.apply(context, args);
          }
        };
        let callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) {
          func.apply(context, args);
        }
      };
    }

    debouncedEventHandler;

    setData() {
      $('#popup').hide();
      $('#rainButton').hide();
      $('#options').fadeIn();

      var input = document.getElementById('autocomplete'),
        autocomplete = new google.maps.places.Autocomplete(input, {
          types: ['(cities)']
        });

      setTimeout(() => {
        // __e3_ is property attached by google script to the input element
        // containing all event listeners
        //get the key for input event listener
        var googleInputEventHandler;
        // loop through the listeners of input event
        for (let key in input.__e3_.input) {
          // check to get reference to the handler function
          // and save it in googleInputEventHandler
          for (let key2 in input.__e3_.input[key]) {
            if (typeof input.__e3_.input[key][key2] === 'function') {
              googleInputEventHandler = input.__e3_.input[key][key2];
              input.removeEventListener('input', googleInputEventHandler);
              break;
            }
          }
        }
        // remove the handler attached by google script

        // re-attach the handler with debounce function
        this.debouncedEventHandler = this.debounce(
          googleInputEventHandler,
          300
        );

        input.addEventListener('input', this.debouncedEventHandler);
      }, 400);

      google.maps.event.addListener(autocomplete, 'place_changed', function() {
        var place = autocomplete.getPlace(), // e - place
          lat = place.geometry.location.lat(), //t-lat
          lng = place.geometry.location.lng(), //o-lng
          address = place.address_components[0].long_name, //a adress longname
          loc = lat + ',' + lng; // l -loc
        (document.getElementById('city').innerHTML = address),
          (document.getElementById('opLocation').innerHTML = loc);
      });
      var city_display_name = localStorage.DisplayName;
      if (city_display_name == null) {
        city_display_name = localStorage.default_DisplayName;
      }
      $('#autocomplete').val(city_display_name),
        $('#city').text(localStorage.city),
        $('#opLocation').text(localStorage.loc);
    }
    setColor(color) {
      this.color = color;
      localStorage.units
        ? localStorage.units === 'c'
          ? $('#celsius').attr('checked', true)
          : $('#fahrenheit').attr('checked', true)
        : null;

      $('#cntWrapper > h1,#unitsWrapper,#locationWrapper,#travelWrapper')
        .css('color', color)
        .fadeIn();
      $('input[type=text]').css('border-bottom', `0.5px solid ${color}`);

      $('input[type=radio]:checked')
        .prev()
        .css('background', color);
      $('#locationWrapper button').css('background', color);
    }

    setTravelWeather() {
      const $travelCheckbox = $('#travelCheckbox');

      if (
        localStorage.allowTravelWeather &&
        localStorage.allowTravelWeather != 'false'
      ) {
        document.querySelector('#travel').checked = true;
        $('#travel').attr('checked', true);
        $travelCheckbox.addClass('checked');
        localStorage.allowTravelWeather = 'true';
      } else {
        document.querySelector('#travel').checked = false;
      }
      $("input[name='travelweather']").click(function() {
        const isChecked = document.querySelector('#travel').checked;
        if (isChecked) {
          $travelCheckbox.addClass('checked');
          $travelCheckbox.css('background', '');
        } else {
          $travelCheckbox.removeClass('checked');
        }
      });
    }

    setFahCel() {
      var self = this;
      $('input[name="units"]').change(function() {
        if (this.checked) {
          localStorage.units = $(this).val();

          $(this)
            .prev()
            .css('background', self.color);
        }
        $('input:not(:checked)')
          .prev()
          .css('background', 'none');
        var temp = parseInt(
          $('#currTemp > h1')
            .text()
            .split('°')[0]
        );
        var currMin = parseInt(
          $('#currMin')
            .text()
            .split('°')[0]
        );
        var currMax = parseInt($('#currMax').text());
        var chartSpans = $('.yLabel')
          .text()
          .split('°')
          .slice(0, 5);
        var weatherContSpans = $('.weatherTemp')
          .text()
          .split('°')
          .slice(0, 5);
        if (localStorage.units === 'c') {
          temp = Math.round(((temp - 32) * 5) / 9); //fahr to cel
          currMin = Math.round(((currMin - 32) * 5) / 9);
          currMax = Math.round(((currMax - 32) * 5) / 9);
          if (!$('#rainButton').attr('class')) {
            chartSpans = chartSpans.map(x => {
              return Math.round(((parseInt(x) - 32) * 5) / 9);
            });
          }

          weatherContSpans = weatherContSpans.map(x => {
            return Math.round(((parseInt(x) - 32) * 5) / 9);
          });
        } else {
          temp = Math.round((temp * 9) / 5 + 32);
          currMin = Math.round((currMin * 9) / 5 + 32);
          currMax = Math.round((currMax * 9) / 5 + 32);
          if (!$('#rainButton').attr('class')) {
            chartSpans = chartSpans.map(x => {
              return Math.round((parseInt(x) * 9) / 5 + 32);
            });
          }

          weatherContSpans = weatherContSpans.map(x => {
            return Math.round((parseInt(x) * 9) / 5 + 32);
          });
        }
        $('#currTemp > h1').text(`${temp}` + '°');
        $('#currMin').text(`${currMin}` + '°');
        $('#currMax').text(` ${currMax}`);
        if (!$('#rainButton').attr('class')) {
          $('.yLabel').each(function(index, value) {
            $(this).text(chartSpans[index] + '°');
          });
        }
        $('.weatherTemp').each(function(index, value) {
          $(this).text(weatherContSpans[index] + '°');
        });
        chrome.browserAction.setBadgeText({
          text: temp + (localStorage.units === 'c' ? '°C' : '°F')
        });
      });
    }

    open(color) {
      this.setData();
      this.setColor(color);

      return new Promise((res, rej) => {
        var self = this;
        var isSet = false;
        $('#save').on('click', function() {
          var allowTravelWeather = $('#travelCheckbox').hasClass('checked')
            ? 'true'
            : 'false';
          localStorage.allowTravelWeather = allowTravelWeather;
          var city = $('#city').text();
          var location = $('#opLocation').text();
          var autocomplete = $('#autocomplete').val();
          if (city && autocomplete) {
            localStorage.city = city;
            localStorage.loc = location;
            localStorage.DisplayName = autocomplete;
            self.data = {};
            self.data.city = city;
            self.data.location = location;
            self.data.autocomplete = autocomplete;
            isSet = true;
            $(
              '#myChart,#currTemp,#conditions,#weather,#chartjs-tooltip,#location'
            ).empty();
            res(self.data);
          }
          $(this).text('Saved');
          setTimeout(function() {
            $('#save').text('Save');
          }, 1000);
          self.close();
          res(null);
        });
        $('#home_btn').click(() => {
          self.close();
          if (!isSet) {
            res(null);
          }
        });
      });
    }
    close() {
      //also remove the input event listener when switching between options in popup
      var input = document.getElementById('autocomplete');
      input.removeEventListener('input', this.debouncedEventHandler);

      $('body').css('height', '526px');
      $('#options').hide();
      $('#rainButton').show();
      $('#popup').fadeIn();
    }
  }
  return OptionsDialog;
});
